package $1_100.$1_10;

public class $1_TwoSum {

    // https://leetcode.com/problems/two-sum/

    public static int[] twoSum(int[] nums, int target) {
        for (int firstNumber = 0; firstNumber < nums.length; firstNumber++) {
            for (int secondNumber = firstNumber + 1; secondNumber < nums.length; secondNumber++) {
                if (nums[firstNumber] + nums[secondNumber] == target) {
                    return new int[]{firstNumber, secondNumber};
                }
            }
        }
        return new int[]{};
    }
}
