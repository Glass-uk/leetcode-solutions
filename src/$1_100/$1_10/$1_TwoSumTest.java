package $1_100.$1_10;

import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

class $1_TwoSumTest {

    @Test
    void Example_1() {
        int[] output = {0, 1};
        assertThat($1_TwoSum.twoSum(new int[]{2, 7, 11, 15}, 9), is(output));
    }

    @Test
    void Example_2() {
        int[] output = {1, 2};
        assertThat($1_TwoSum.twoSum(new int[]{3, 2, 4}, 6), is(output));
    }

    @Test
    void Example_3() {
        int[] output = {0, 1};
        assertThat($1_TwoSum.twoSum(new int[]{3, 3}, 6), is(output));
    }
}
